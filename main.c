//#############################################################################
// $TI Release: MotorControl SDK v3.01.00.00 $
// $Release Date: Mon Jan 11 11:23:03 CST 2021 $
// $Copyright:
// Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//   Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the
//   distribution.
//
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//**************************************************************************
//
//! \file   solutions/common/sensorless_foc/source/is01_intro_hal.c
//! \brief  This lab uses the HAL object to setup the inverter
//!  hardware based C2000 controllers with LED blinking
//
//**************************************************************************

//
// solutions
//
#include "labs.h"
#include "drv8302.h" // Added by jjkhan
#include "canapp.h"
#include "uartapp.h"

#pragma CODE_SECTION(mainISR, ".TI.ramfunc");

//
// the globals
//

HAL_ADCData_t adcData = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, 0.0};

HAL_PWMData_t pwmData = {{0.0, 0.0, 0.0}};

uint16_t counterLED = 0;  //!< Counter used to divide down the ISR rate for
                           //!< visually blinking an LED

uint16_t counterTrajId = 0;

uint32_t offsetCalcCount = 0;      //!< Counter used to count the wait time
                                   //!< for offset calibration, unit: ISR cycles

uint32_t offsetCalcWaitTime = 500;  //!< Wait time setting for current/voltage
                                      //!< offset calibration, unit: ISR cycles

float32_t angleDelta_rad;   //!< the rotor angle compensation value
float32_t angleFoc_rad;     //!< the rotor angle for FOC modules

MATH_Vec2 Idq_ref_A;        //!< the reference current on d&q rotation axis
MATH_Vec2 Idq_offset_A;     //!< the offsetting current on d&q rotation axis

MATH_Vec2 Vab_out_V;        //!< the output control voltage on alpha&beta axis
MATH_Vec2 Vdq_out_V;        //!< the output control voltage on d&q axis

MATH_Vec2 Iab_in_A;         //!< the input current on alpha&beta axis
MATH_Vec2 Idq_in_A;         //!< the input current on d&q rotation axis
MATH_Vec2 IdqSet_A = {0.0, 0.0};    //!< the desired user input current on d&q rotation axis

USER_Params userParams;    //!< the user parameters for motor control
                            //!< and hardware board configuration
#pragma DATA_SECTION(userParams, "ctrl_data");

volatile MOTOR_Vars_t motorVars = MOTOR_VARS_INIT; // Initialize Motor Parameters
#pragma DATA_SECTION(motorVars, "ctrl_data");

CTRL_Handle   ctrlHandle;       //!< the handle for the controller
CTRL_Obj      ctrl;             //!< the controller object

CLARKE_Handle clarkeHandle_I;   //!< the handle for the current Clarke transform
CLARKE_Obj    clarke_I;         //!< the current Clarke transform object

CLARKE_Handle clarkeHandle_V;   //!< the handle for the voltage Clarke transform
CLARKE_Obj    clarke_V;         //!< the voltage Clarke transform object

HAL_Handle    halHandle;     //!< the handle for the hardware abstraction layer
HAL_Obj       hal;           //!< the hardware abstraction layer object

IPARK_Handle  iparkHandle;      //!< the handle for the inverse Park transform
IPARK_Obj     ipark;            //!< the inverse Park transform object

PARK_Handle   parkHandle;       //!< the handle for the Park object
PARK_Obj      park;             //!< the Park transform object

PI_Handle     piHandle_Id;      //!< the handle for the Id PI controller
PI_Obj        pi_Id;            //!< the Id PI controller object

PI_Handle     piHandle_Iq;      //!< the handle for the Iq PI controller
PI_Obj        pi_Iq;            //!< the Iq PI controller object

SVGEN_Handle  svgenHandle;      //!< the handle for the space vector generator
SVGEN_Obj     svgen;            //!< the space vector generator object

TRAJ_Handle   trajHandle_Id;    //!< the handle for the id reference trajectory
TRAJ_Obj      traj_Id;          //!< the id reference trajectory object

//!< the handles for the current offset calculation
FILTER_FO_Handle  filterHandle_I[USER_NUM_CURRENT_SENSORS];
//!< the current offset calculation
FILTER_FO_Obj     filter_I[USER_NUM_CURRENT_SENSORS];



// For Digital Oscilloscope
#ifdef PWMDAC_ENABLE
HAL_PWMDACData_t pwmDACData;
#pragma DATA_SECTION(pwmDACData, "ctrl_data");
#endif

bool flagEnableOffsetCalibration = false; //!< the flag for enabling offset calibration
bool moduleFault;  // For Gate Driver Fault pin
static inline void SystemEnabled(void); // while-loop operations
// Uncomment this when QEP is configured to read encoder values
// Also, in mainISR need to read the encoder value in for forward and inverse Park Transformation
//#define QEP_CONFIGURED

// Connection Information
/* Sensing phase currents and Bus voltage
    ISENA (J5 connector, pin 49)  -> PGA2->A14->RA0
    ISENB (J5 connector, pin 47)  -> PGA4->B7->RB0
    ISENC (J5 connector, pin 48)  -> PGA6->C7->RC0
    VdcBus (J3 Connector, pin 26) -> B1->RB1.
*/

// Note: Make sure you have the correct Motor parameters set.
//  "MOTOR_VARS_INIT" macro has the Motor initial parameters, such as DC bus voltage, PI gain values.

//
// the functions
//
void main(void)
{
    //
    // initialize the user parameters
    //
    USER_setParams(&userParams);

    userParams.flag_bypassMotorId = true;

    //
    // initialize the user parameters
    //
    USER_setParams_priv(&userParams);

    //
    // initialize the driver
    //
    halHandle = HAL_init(&hal, sizeof(hal));

    //
    // set the driver parameters
    //
    HAL_setParams(halHandle);

    //
    // initialize the Clarke modules
    //
    clarkeHandle_I = CLARKE_init(&clarke_I, sizeof(clarke_I));

    //
    // set the Clarke parameters
    //
    setupClarke_I(clarkeHandle_I, userParams.numCurrentSensors);

    //
    // initialize the controller
    //
    ctrlHandle = CTRL_init(&(ctrl), sizeof(ctrl));

    //
    // set the default controller parameters
    //
    CTRL_setParams(ctrlHandle, &userParams);

    //
    // initialize the inverse Park module
    //
    iparkHandle = IPARK_init(&ipark, sizeof(ipark));

    //
    // initialize the Park module
    //
    parkHandle = PARK_init(&park, sizeof(park));

    //
    // initialize the PI controllers
    //
    piHandle_Id  = PI_init(&pi_Id, sizeof(pi_Id));
    piHandle_Iq  = PI_init(&pi_Iq, sizeof(pi_Iq));

    //
    // setup the current controllers d/q-axis current pid regulator
    //
    setupCurrentControllers();

    //
    // initialize the space vector generator module
    //
    svgenHandle = SVGEN_init(&svgen, sizeof(svgen));

    //
    // initialize the Id reference trajectory
    //
    trajHandle_Id = TRAJ_init(&traj_Id, sizeof(traj_Id));

    //
    // configure the Id reference trajectory
    //
    TRAJ_setTargetValue(trajHandle_Id, 0.0);
    TRAJ_setIntValue(trajHandle_Id, 0.0);
    TRAJ_setMinValue(trajHandle_Id, -USER_MOTOR_MAX_CURRENT_A);
    TRAJ_setMaxValue(trajHandle_Id,  USER_MOTOR_MAX_CURRENT_A);
    TRAJ_setMaxDelta(trajHandle_Id,
                     (USER_MOTOR_RES_EST_CURRENT_A / USER_ISR_FREQ_Hz));

    /*
         You only need to run offset calibration for your hardware interface once
            then save the calibration result in user.h in the structure members
            offsets_I_A and offsets_V_V in motorVars structure.
    */

    //
    // initialize and configure offsets using first-order filter
    //
    {
        //
        // Sets the first-order filter denominator coefficients
        // a1, the filter coefficient value for z^(-1)
        // b0, the filter coefficient value for z^0
        // b1, the filter coefficient value for z^(-1)
        //
        uint16_t cnt = 0;
        float32_t b0 = userParams.offsetPole_rps / userParams.ctrlFreq_Hz;
        float32_t a1 = (b0 - 1.0);
        float32_t b1 = 0.0;

        //
        // For Current offset calibration filter
        //
        for(cnt = 0; cnt < USER_NUM_CURRENT_SENSORS; cnt++)
        {
            filterHandle_I[cnt] = FILTER_FO_init(&filter_I[cnt],
                                                   sizeof(filter_I[cnt]));

            FILTER_FO_setDenCoeffs(filterHandle_I[cnt], a1);
            FILTER_FO_setNumCoeffs(filterHandle_I[cnt], b0, b1);

            FILTER_FO_setInitialConditions(filterHandle_I[cnt],
                                        motorVars.offsets_I_A.value[cnt],
                                        motorVars.offsets_I_A.value[cnt]);
        }
        //++ Added by jjkhan: Once ADC offset calibration is done first-time,
            // And you've saved the offset results in the user.h file, in the respective structure members
                    // set the following flag to false.
        motorVars.flagEnableOffsetCalc = false;
        offsetCalcCount = 0;
    }


#ifdef PWMDAC_ENABLE
    // set DAC parameters
    pwmDACData.periodMax =
            PWMDAC_getPeriod(halHandle->pwmDACHandle[PWMDAC_NUMBER_1]);

    pwmDACData.ptrData[0] = &adcData.I_A.value[0];
    pwmDACData.ptrData[1] = &adcData.I_A.value[1];
    pwmDACData.ptrData[2] = &adcData.V_V.value[0];
    pwmDACData.ptrData[3] = &adcData.V_V.value[1];

    pwmDACData.offset[0] = 1.0;     // Change -1.0~1.0 to 0~1.0 for pwmDac
    pwmDACData.offset[1] = 0.5;     // Change -1.0~1.0 to 0~1.0 for pwmDac
    pwmDACData.offset[2] = 0.5;     // Change -1.0~1.0 to 0~1.0 for pwmDac
    pwmDACData.offset[3] = 0.5;     // Change -1.0~1.0 to 0~1.0 for pwmDac

    pwmDACData.gain[0] = -MATH_ONE_OVER_TWO_PI;  // Convert -PI()~PI() to 0~1.0
    pwmDACData.gain[1] = 1.0 / USER_ADC_FULL_SCALE_CURRENT_A;
    pwmDACData.gain[2] = 1.0 / USER_ADC_FULL_SCALE_VOLTAGE_V;
    pwmDACData.gain[3] = 1.0 / USER_ADC_FULL_SCALE_CURRENT_A;
#endif  // PWMDAC_ENABLE

#if DATALOG_ENABLE
    // Initialize Datalog
    datalogHandle = DATALOG_init(&datalog, sizeof(datalog));
    DATALOG_Obj *datalogObj = (DATALOG_Obj *)datalogHandle;

    HAL_resetDlogWithDMA();
    HAL_setupDlogWithDMA(halHandle, 0, &datalogBuff1[0], &datalogBuff1[1]);
    HAL_setupDlogWithDMA(halHandle, 1, &datalogBuff2[0], &datalogBuff2[1]);
    #if (DATA_LOG_BUFF_NUM == 4)
    HAL_setupDlogWithDMA(halHandle, 2, &datalogBuff3[0], &datalogBuff3[1]);
    HAL_setupDlogWithDMA(halHandle, 3, &datalogBuff4[0], &datalogBuff4[1]);
    #endif  // DATA_LOG_BUFF_NUM == 4

    // set datalog parameters
    datalogObj->iptr[0] = &adcData.I_A.value[0];
    datalogObj->iptr[1] = &adcData.I_A.value[1];
    #if (DATA_LOG_BUFF_NUM == 4)
    datalogObj->iptr[2] = &adcData.V_V.value[0];
    datalogObj->iptr[3] = &adcData.V_V.value[1];
    #endif  // DATA_LOG_BUFF_NUM == 4
#endif  // DATALOG_ENABLE

    //
    // setup faults
    //
    HAL_setupFaults(halHandle);

    //
    // disable the PWM
    //
    HAL_disablePWM(halHandle);

// ++Added by jjkhan
#if (BOOST_to_LPD == SPECTRE_MOTION)
    //
    // turn on the DRV8302 if present
    //
    HAL_enableDRV(halHandle);
#endif
//-- Added by jjkhan

    //
    // Set some global variables
    //
    motorVars.pwmISRCount = 0;     // clear the counter
    motorVars.speedRef_Hz = 20.0;  // set the default frequency to 20.0Hz

    //
    // Set the current for torque control
    //
    IdqSet_A.value[0] = 0.0;        //
    IdqSet_A.value[1] = 0.0;        // For PMSM, flux is fixed, so don't need Iq current

    //
    // disable the PWM
    //
    HAL_disablePWM(halHandle);

    //
    // initialize the interrupt vector table
    //
    HAL_initIntVectorTable(halHandle);

    //
    // enable the ADC interrupts
    //
    HAL_enableADCInts(halHandle);

    //
    // disable global interrupts
    //
    HAL_enableGlobalInts(halHandle);

    //
    // enable debug interrupts
    //
    HAL_enableDebugInt(halHandle);

    CanApp_init();
    UartApp_init();

    // Uncomment to test JSON message over UART in infinite loop
    //UartApp_testJsonComms();
    //
    // Waiting for enable system flag to be set
    //
    while(motorVars.flagEnableSys == false)
    {

    }

    //
    // loop while the enable system flag is true
    //
    while(motorVars.flagEnableSys == true)
    {
        SystemEnabled();

    } // end of while() loop

    //
    // disable the PWM
    //
    HAL_disablePWM(halHandle);

} // end of main() function

static inline void SystemEnabled(void){
    //
    // 1ms time base
    //
    if (HAL_getTimerStatus(halHandle, HAL_CPU_TIMER1))
    {
        motorVars.timerCnt_1ms++;

        HAL_clearTimerFlag(halHandle, HAL_CPU_TIMER1);
    }

    motorVars.mainLoopCount++;

    //
    // set the reference value for internal DACA and DACB
    //
    HAL_setDACValue(halHandle, 0, motorVars.dacaVal);
    HAL_setDACValue(halHandle, 1, motorVars.dacbVal);

    //
    // set internal DAC value for on-chip comparator for current protection
    //
    {
        uint16_t cmpssCnt;

        for (cmpssCnt = 0; cmpssCnt < HAL_NUM_CMPSS_CURRENT; cmpssCnt++)
        {
            HAL_setCMPSSDACValueHigh(halHandle, cmpssCnt, motorVars.dacValH);

            HAL_setCMPSSDACValueLow(halHandle, cmpssCnt, motorVars.dacValL);
        }
    }

    if (HAL_getPwmEnableStatus(halHandle) == true)
    {
        if (HAL_getTripFaults(halHandle) != 0)
        {
            motorVars.faultNow.bit.moduleOverCurrent = 1;
        }
    }

    motorVars.faultUse.all = motorVars.faultNow.all & motorVars.faultMask.all;

    //
    // Had some faults to stop the motor
    //
    if (motorVars.faultUse.all != 0)
    {
        motorVars.flagRunIdentAndOnLine = 0;
    }

    if (motorVars.flagEnableOffsetCalc == false)
    {
        //
        // disable the PWM
        //
        HAL_disablePWM(halHandle);

        //
        // clear integral outputs of the controllers
        //
        PI_setUi(piHandle_Id, 0.0);
        PI_setUi(piHandle_Iq, 0.0);

        //
        // clear current references
        //
        Idq_ref_A.value[0] = 0.0;
        Idq_ref_A.value[1] = 0.0;

        //
        // clear current offsets
        //
        Idq_offset_A.value[0] = 0.0;
        Idq_offset_A.value[1] = 0.0;

        //
        // clear current and angle for FWC and MTPA
        //  MTPA = Maximum Torque Per Ampere
        //  FWC  = Field Weakening Control
        //
        motorVars.VsRef_pu = USER_MAX_VS_MAG_PU;
        motorVars.IsRef_A = 0.0;
        motorVars.angleCurrent_rad = 0.0;

    }

    if (motorVars.flagMotorIdentified == true)
    {
        if(motorVars.flagSetupController == true){
            //
            // update the controller
            // set custom current and speed controllers gains
            //
            updateControllers();
        }else{

            motorVars.flagMotorIdentified = true;
            motorVars.flagSetupController = true;

            //setupControllers();
            setupCurrentControllers();
        }

    }

    //++ Added by jjkhan
#if (BOOST_to_LPD == SPECTRE_MOTION)
    // Read fault pin of Power Module
    moduleFault = GPIO_readPin(HAL_PM_nFAULT_GPIO);
    // -- Added by jjkhan
#endif
} // End of SystemEnabled() function

__interrupt void mainISR(void)
{
    motorVars.pwmISRCount++;

    //
    // toggle status LED
    //
    counterLED++;

    if(counterLED > (uint32_t)(USER_ISR_FREQ_Hz / LED_BLINK_FREQ_Hz))
    {
        HAL_toggleLED(halHandle, HAL_GPIO_LED2);
        counterLED = 0;
    }

    //
    // acknowledge the ADC interrupt
    //
    HAL_ackADCInt(halHandle, ADC_INT_NUMBER1);

    //
    // read the ADC data with offsets
    //
    HAL_readADCDataWithOffsets(halHandle, &adcData);

    //
    // remove offsets
    //
    adcData.I_A.value[0] -= motorVars.offsets_I_A.value[0];
    adcData.I_A.value[1] -= motorVars.offsets_I_A.value[1];
    adcData.I_A.value[2] -= motorVars.offsets_I_A.value[2];


    // ++ Added by jjkhan
        // The following is where the hardware (mainly ADC) offset calibration takes place
    if(motorVars.flagEnableOffsetCalc == false){
        //
        // Verifies torque control with current close loop

        float32_t outMax_V;
        MATH_Vec2 phasor;
        float32_t oneOverDcBus_invV;

        //
        // run Clarke transform on current
        //
        CLARKE_run(clarkeHandle_I, &adcData.I_A, &(Iab_in_A));

        // Read Rotor Angle from QEP
        //
// ++ Added by jjkhan
#if QEP_CONFIGURED
        // @Mahmoud Read Rotor Angle using QEP

        // Replace "estOutputData.fm_lp_rps" with Mechanical Frequency (rad/sec) -> Determined from Angle information
        angleDelta_rad = userParams.angleDelayed_sf_sec *
                                 estOutputData.fm_lp_rps;

        // Replace "estOutputData.angle_rad" with Angle information from QEP
        angleFoc_rad = MATH_incrAngle(estOutputData.angle_rad,
                                            angleDelta_rad);
#else
        angleFoc_rad = 0.0; // To avoid build issues.
#endif
// -- Added by jjkhan

        //
        // compute the sin/cos phasor using fast RTS function, callable assembly
        //
        phasor.value[0] = cosf(angleFoc_rad);
        phasor.value[1] = sinf(angleFoc_rad);

        //
        // set the phasor in the Park transform
        //
        PARK_setPhasor(parkHandle, &phasor);

        //
        // run the forward Park module
        //
        PARK_run(parkHandle, &(Iab_in_A), &(Idq_in_A));

        // set reference current
        Idq_ref_A.value[0] = IdqSet_A.value[0];
        Idq_ref_A.value[1] = IdqSet_A.value[1];

        //
        // Maximum voltage output
        //
        userParams.maxVsMag_V = userParams.maxVsMag_pu * adcData.dcBus_V;
        PI_setMinMax(piHandle_Id,
                     (-userParams.maxVsMag_V), userParams.maxVsMag_V);

        //
        // run the Id controller
        //
        PI_run_series(piHandle_Id,
                      Idq_ref_A.value[0] + Idq_offset_A.value[0],
                      Idq_in_A.value[0],
                      0.0,
                      &(Vdq_out_V.value[0]));

        //
        // calculate Iq controller limits, and run Iq controller using fast RTS
        // function, callable assembly
        //
        outMax_V = sqrt((userParams.maxVsMag_V * userParams.maxVsMag_V) -
                        (Vdq_out_V.value[0] * Vdq_out_V.value[0]));

        PI_setMinMax(piHandle_Iq, -outMax_V, outMax_V);
        PI_run_series(piHandle_Iq,
                      Idq_ref_A.value[1] + Idq_offset_A.value[1],
                      Idq_in_A.value[1],
                      0.0,
                      &(Vdq_out_V.value[1]));

        //
        // compute angle with delay compensation
        //
// ++ Added by jjkhan
#if QEP_CONFIGURED
        // @Mahmoud Add compensation for the delay caused by the ISR handling for output

        // Replace "estOutputData.fm_lp_rps" with Mechanical Frequency (rad/sec) -> Determined from Angle information
        angleDelta_rad = userParams.angleDelayed_sf_sec *
                                 estOutputData.fm_lp_rps;

        // Replace "estOutputData.angle_rad" with Angle information from QEP
        angleFoc_rad = MATH_incrAngle(estOutputData.angle_rad,
                                            angleDelta_rad);
#else
        angleFoc_rad = 0.0; // To avoid build issues.
#endif
// -- Added by jjkhan
        //
        // compute the sin/cos phasor using fast RTS function, callable assembly
        //
        phasor.value[0] = cosf(angleFoc_rad);
        phasor.value[1] = sinf(angleFoc_rad);

        //
        // set the phasor in the inverse Park transform
        //
        IPARK_setPhasor(iparkHandle, &phasor);

        //
        // run the inverse Park module
        //
        IPARK_run(iparkHandle, &Vdq_out_V, &Vab_out_V);

        //
        // setup the space vector generator (SVGEN) module
        //
        // ++ Added by jjkhan
        oneOverDcBus_invV = (1.0f)/(adcData.dcBus_V);
        SVGEN_setup(svgenHandle, oneOverDcBus_invV);
        // -- Added by jjkhan
        //SVGEN_setup(svgenHandle, estOutputData.oneOverDcBus_invV);

        //
        // run the space vector generator (SVGEN) module
        //
        SVGEN_run(svgenHandle, &Vab_out_V, &(pwmData.Vabc_pu));
    }
    else if(motorVars.flagEnableOffsetCalc == true)
        {
            runOffsetsCalculation();
    }

    if(HAL_getPwmEnableStatus(halHandle) == false)
    {
        //
        // clear PWM data
        //
        pwmData.Vabc_pu.value[0] = 0.0;
        pwmData.Vabc_pu.value[1] = 0.0;
        pwmData.Vabc_pu.value[2] = 0.0;
    }

    //
    // write the PWM compare values
    //
    HAL_writePWMData(halHandle, &pwmData);

#ifdef PWMDAC_ENABLE
    //
    // connect inputs of the PWMDAC module
    //
    HAL_writePWMDACData(halHandle, &pwmDACData);
#endif  // PWMDAC_ENABLE

#ifdef DATALOG_ENABLE
    //
    // connect inputs of the DATALOG module
    //
    DATALOG_updateWithDMA(datalogHandle);

    // Force trig DMA channel to save the data
    HAL_trigDlogWithDMA(halHandle, 0);
    HAL_trigDlogWithDMA(halHandle, 1);
    HAL_trigDlogWithDMA(halHandle, 2);
    HAL_trigDlogWithDMA(halHandle, 3);
#endif  //  DATALOG_ENABLE


    return;
} // end of mainISR() function

/*
 *   Added by jjkhan:
 *      The following function does the actual calibration and saves the offset values in the offset_I_A and offset_V_V members of the motorVars structure.
 */
void runOffsetsCalculation(void)
{
    uint16_t cnt;

    if(motorVars.flagEnableSys == true)
    {
        //
        // enable the PWM
        //
        HAL_enablePWM(halHandle);

        //
        // set the 3-phase output PWMs to 50% duty cycle
        //
        pwmData.Vabc_pu.value[0] = 0.0;
        pwmData.Vabc_pu.value[1] = 0.0;
        pwmData.Vabc_pu.value[2] = 0.0;

        for(cnt = 0; cnt < USER_NUM_CURRENT_SENSORS; cnt++)
        {
            //
            // reset current offsets used
            //
            motorVars.offsets_I_A.value[cnt] = 0.0;

            //
            // run current offset estimation
            //
            FILTER_FO_run(filterHandle_I[cnt],
                          adcData.I_A.value[cnt]);
        }

        offsetCalcCount++; // counts the wait time for offset calibration: unit = ISR cycles

        if(offsetCalcCount >= offsetCalcWaitTime)
        {
            for(cnt = 0; cnt < USER_NUM_CURRENT_SENSORS; cnt++)
            {
                //
                // get calculated current offsets from filter
                //
                motorVars.offsets_I_A.value[cnt] =
                        FILTER_FO_get_y1(filterHandle_I[cnt]);

                //
                // clear current filters
                //
                FILTER_FO_setInitialConditions(filterHandle_I[cnt],
                                              motorVars.offsets_I_A.value[cnt],
                                              motorVars.offsets_I_A.value[cnt]);
            }

            offsetCalcCount = 0;
            motorVars.flagEnableOffsetCalc = false;

            //
            // disable the PWM
            //
            HAL_disablePWM(halHandle);
        }
    }

    return;
} // end of runOffsetsCalculation() function

//
// end of file
//
