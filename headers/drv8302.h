/*
 * drv8302.h
 *
 *  Created on: May 20, 2021
 *      Author: junaidkhan
 */

#ifndef HEADERS_DRV8302_H_
#define HEADERS_DRV8302_H_

#define DRV8302
#define DRV8302_FAULT       1U   // Confirm with datasheet

//! \brief Defines the DRV8302 object
//!
typedef struct _DRV8302_Obj_
{
    uint32_t  gpioNumber_EN;        //!< GPIO connected to the DRV8320 enable pin
    uint32_t  gpioNumber_FLT;        //!< GPIO connected to the DRV8320 Fault pin
    bool      enableTimeOut;        //!< timeout flag for DRV8302 enable

} DRV8302_Obj;

//! \brief Defines the DRV8302 handle
//!
typedef struct _DRV8302_Obj_ *DRV8302_Handle;


// **************************************************************************
// the globals

// **************************************************************************
// the function prototypes

//! \brief     Initializes the DRV8302 object
//! \param[in] pMemory   A pointer to the memory for the DRV8302 object
//! \param[in] numBytes  The number of bytes allocated for the DRV8302
//!                      object, bytes
//! \return    The DRV8302 object handle
extern DRV8302_Handle DRV8302_init(void *pMemory);

//! \brief     Enables the DRV8302
//! \param[in] handle     The DRV8302 handle
extern void DRV8302_enable(DRV8302_Handle handle);

//! \brief     Sets the GPIO number in the DRV8302
//! \param[in] handle       The DRV8302 handle
//! \param[in] gpioHandle   The GPIO number to use
void DRV8302_setGPIONumber(DRV8302_Handle handle,uint32_t gpioNumber_EN, uint32_t gpioNumber_FAULT);

#endif /* HEADERS_DRV8302_H_ */


