/*
 * uartapp.c
 *
 *  Created on: May 18, 2021
 *      Author: mkamalel
 */

//! \file   ~/spectre/middle_layer/source/canmiddle.c
//! \brief  Contains the middle layer of CAN
//! \brief  CanMiddle (CanMiddle)
//!

#include "sci.h"
#include "gpio.h"
#include "string.h"
#include "stdio.h"
#include "uartapp.h"
#include "device.h"
#include "cJSON.h"

#define PC_UART SCIA_BASE
#define UART_BAUDRATE 115200

float sine[] = {2048,2098,2148,2199,2249,2299,2349,2398,
2448,2497,2546,2594,2643,2690,2738,2785,
2832,2878,2924,2969,3013,3057,3101,3144,
3186,3227,3268,3308,3347,3386,3423,3460,
3496,3531,3565,3599,3631,3663,3693,3722,
3751,3778,3805,3830,3854,3877,3899,3920,
3940,3959,3976,3993,4008,4022,4035,4046,
4057,4066,4074,4081,4086,4090,4094,4095,
4096,4095,4094,4090,4086,4081,4074,4066,
4057,4046,4035,4022,4008,3993,3976,3959,
3940,3920,3899,3877,3854,3830,3805,3778,
3751,3722,3693,3663,3631,3599,3565,3531,
3496,3460,3423,3386,3347,3308,3268,3227,
3186,3144,3101,3057,3013,2969,2924,2878,
2832,2785,2738,2690,2643,2594,2546,2497,
2448,2398,2349,2299,2249,2199,2148,2098,
2048,1998,1948,1897,1847,1797,1747,1698,
1648,1599,1550,1502,1453,1406,1358,1311,
1264,1218,1172,1127,1083,1039,995,952,
910,869,828,788,749,710,673,636,
600,565,531,497,465,433,403,374,
345,318,291,266,242,219,197,176,
156,137,120,103,88,74,61,50,
39,30,22,15,10,6,2,1,
0,1,2,6,10,15,22,30,
39,50,61,74,88,103,120,137,
156,176,197,219,242,266,291,318,
345,374,403,433,465,497,531,565,
600,636,673,710,749,788,828,869,
910,952,995,1039,1083,1127,1172,1218,
1264,1311,1358,1406,1453,1502,1550,1599,
1648,1698,1747,1797,1847,1897,1948,1998};

void UartApp_send(uint32_t sci, char data[])
{
    char *first = &data[0];
    SCI_writeCharArray(sci, (uint16_t *)first, strlen(data));
}

void UartApp_init()
{
  // GPIO28 is the SCI Rx pin.
  //
  GPIO_setMasterCore(DEVICE_GPIO_PIN_SCIRXDA, GPIO_CORE_CPU1);
  GPIO_setPinConfig(DEVICE_GPIO_CFG_SCIRXDA);
  GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_DIR_MODE_IN);
  GPIO_setPadConfig(DEVICE_GPIO_PIN_SCIRXDA, GPIO_PIN_TYPE_STD);
  GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCIRXDA, GPIO_QUAL_ASYNC);

  //
  // GPIO29 is the SCI Tx pin.
  //
  GPIO_setMasterCore(DEVICE_GPIO_PIN_SCITXDA, GPIO_CORE_CPU1);
  GPIO_setPinConfig(DEVICE_GPIO_CFG_SCITXDA);
  GPIO_setDirectionMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_DIR_MODE_OUT);
  GPIO_setPadConfig(DEVICE_GPIO_PIN_SCITXDA, GPIO_PIN_TYPE_STD);
  GPIO_setQualificationMode(DEVICE_GPIO_PIN_SCITXDA, GPIO_QUAL_ASYNC);

  //
  // Initialize interrupt controller and vector table.
  //
  Interrupt_initModule();
  Interrupt_initVectorTable();

  //
  // Initialize SCIA and its FIFO.
  //
  SCI_performSoftwareReset(SCIA_BASE);

  //
  // Configure SCIA for echoback.
  //
  SCI_setConfig(SCIA_BASE, DEVICE_LSPCLK_FREQ, UART_BAUDRATE, (SCI_CONFIG_WLEN_8 |
                                                      SCI_CONFIG_STOP_ONE |
                                                      SCI_CONFIG_PAR_NONE));
  SCI_resetChannels(SCIA_BASE);
  SCI_resetRxFIFO(SCIA_BASE);
  SCI_resetTxFIFO(SCIA_BASE);
  SCI_clearInterruptStatus(SCIA_BASE, SCI_INT_TXFF | SCI_INT_RXFF);
  SCI_enableFIFO(SCIA_BASE);
  SCI_enableModule(SCIA_BASE);
  SCI_performSoftwareReset(SCIA_BASE);
}

void UartApp_printf(const char *_format, ...)
{
   char str[128];
   int16_t length = -1;

   va_list argList;
   va_start( argList, _format );

   length = vsnprintf(str, sizeof(str), _format, argList);

   va_end( argList );

   if (length > 0)
   {
       SCI_writeCharArray(PC_UART, (uint16_t*)str, (unsigned)length);
   }
}

void UartApp_messageFrame(const char *_format, ...)
{
   char str[128];
   int16_t length = -1;
   str[0] = START_BYTE;

   va_list argList;
   va_start( argList, _format );

   length = vsnprintf(&str[1], sizeof(str), _format, argList);

   va_end( argList );

   if (length > 0)
   {
       str[length+1] = DATA_END_BYTE;
       SCI_writeCharArray(PC_UART, (uint16_t*)str, (unsigned)length+2);
   }
}

void UartApp_createPwmMessage(char* string, float chan_a, float chan_b, float chan_c)
{
    //char *string = NULL;
    cJSON *a = NULL;
    cJSON *b = NULL;
    cJSON *c = NULL;

    cJSON *pwm = cJSON_CreateObject();
    if (pwm == NULL)
    {
        goto end;
    }

    a = cJSON_CreateNumber(chan_a);
    if (a == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transferring ownership of the pointer to it */
    cJSON_AddItemToObject(pwm, "A", a);

    b = cJSON_CreateNumber(chan_b);
    if (b == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transferring ownership of the pointer to it */
    cJSON_AddItemToObject(pwm, "B", b);

    c = cJSON_CreateNumber(chan_c);
    if (c == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transferring ownership of the pointer to it */
    cJSON_AddItemToObject(pwm, "C", c);

    //string = cJSON_Print(pwm);
    cJSON_PrintPreallocated(pwm, string, MAX_JSON_LEN, false);
    end:
        cJSON_Delete(pwm);
        //return string;
}

void UartApp_testJsonComms()
{
    uint16_t a = 0;
    uint16_t b = 20;
    uint16_t c = 40;
    char str[100];
    uint16_t len = sizeof(sine)/sizeof(sine[0]);

    for(;;)
    {
        UartApp_createPwmMessage(str, sine[a], sine[b], sine[c]);

        UartApp_messageFrame("%s", str);

        //free(str)
        DEVICE_DELAY_US(100000);
        a+=1;
        b+=1;
        c+=1;

        if(a >= len)
        {
            a = 0;
        }

        if(b >= len)
        {
            b = 0;
        }

        if(c >= len)
        {
            c = 0;
        }

    }
}
