/*
 * canapp.c
 *
 *  Created on: Mar. 22, 2021
 *      Author: mkamalel
 */

//! \file   ~/spectre/application_layersource/can/app.c
//! \brief  Contains the application layer of CAN
//! \brief  CanApp (CanApp)
//!

#include "canapp.h"
#include <stdint.h>
#include "canmiddle.h"
#include "stdio.h"

uint16_t CanApp_init()
{
    CanMiddle_init();

    return 0;
}

uint16_t CanApp_processMessage(uint16_t msgId, uint16_t *rxMsgData)
{
    uint16_t ret = 0;

    // TODO: Check what msg is received and process accordingly
    if(msgId == TORQUE_REQUEST_MSG_ID)
    {
        printf("Torque request\n\r");
    }
    if(msgId == STATUS_QUERY_MSG_ID)
    {
        // Get the received message
        printf("status query\n\r");
    }
    else if(msgId == ALIVE_MSG_ID)
    {
        printf("Alive msg\n\r");
    }
    else if(msgId == PARAMETER_SETTING_MSG_ID)
    {
        printf("Parameter setting\n\r");
    }
    else if(msgId == SHUTDOWN_MSG_ID)
    {
        printf("Shutdown\n\r");
    }
    else
    {
        ret = 0;
    }

    return ret;
}

uint16_t CanApp_sendStatusUpdate()
{
    CanMiddle_sendStatusUpdate();

    return 0;
}
