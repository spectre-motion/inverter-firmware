/*
 * canapp.h
 *
 *  Created on: Mar. 22, 2021
 *      Author: mkamalel
 */

//! \file   ~/spectre/application_layer/include/canapp.h
//! \brief  Contains the application layer of CAN
//! \brief  CanApp (CanApp)
//!

#ifndef SPECTRE_APPLICATION_LAYER_INCLUDE_CANAPP_H_
#define SPECTRE_APPLICATION_LAYER_INCLUDE_CANAPP_H_

#include "stdint.h"
#include "canmiddle.h"
#include "device.h"

//*****************************************************************************
//
//! Initializes Application layer of CAN
//!
//! \param None
//!
//! Sets up GPIO pins for CAN, sets up interrupts, creates message boxes for the
//! CAN library of the Spectre X90, and starts the CAN peripheral
//!
//! \return Returns the value in the data register for the specified pin.
//
//*****************************************************************************
uint16_t CanApp_init();

//*****************************************************************************
//
//! Process received CAN messages
//!
//! \param[in] msgId   ID of message received
//! \param[in] *rxMsgData  Pointer to received data
//!
//! Process received CAN messages to perform different functionalities based on message ID
//!
//! \return Returns the value in the data register for the specified pin.
//
//*****************************************************************************
uint16_t CanApp_processMessage(uint16_t msgId, uint16_t *rxMsgData);

//*****************************************************************************
//
//! Initializes middle layer of CAN
//!
//! \param None
//!
//! Send Status update CAN message
//!
//! \return Status of CAN message send
//
//*****************************************************************************
uint16_t CanApp_sendStatusUpdate();

#endif /* SPECTRE_APPLICATION_LAYER_INCLUDE_CANAPP_H_ */
