/*
 * uartapp.h
 *
 *  Created on: May 18, 2021
 *      Author: mkamalel
 */

#ifndef SPECTRE_APPLICATION_LAYER_INCLUDE_UARTAPP_H_
#define SPECTRE_APPLICATION_LAYER_INCLUDE_UARTAPP_H_

#include "sci.h"

#ifdef __cplusplus
extern "C" {
#endif


#define START_BYTE '\1'
#define DATA_END_BYTE '\2'
#define CRC_END_BYTE '\3'
#define MAX_JSON_LEN 100

//*****************************************************************************
//
//! Initializes application layer of UART
//!
//! \param uint32_t baudrate
//!
//! Sets up SCI peripheral to communicate with PC at specified baudrate
//!
//! \return None
//
//*****************************************************************************
void UartApp_init();

//*****************************************************************************
//
//! Send character array over UART
//!
//! \param sci Base address
//! \param character array
//!
//! Snd character array over UART
//!
//! \return None
//
//*****************************************************************************
void UartApp_send(uint32_t sci, char data[]);

//*****************************************************************************
//
//! printf implemented over UART
//!
//! \param character array
//! \param args
//!
//! Print character array with args over UART
//!
//! \return None
//
//*****************************************************************************
void UartApp_printf(const char *_format, ...);

//*****************************************************************************
//
//! Create message frame for message over UART
//!
//! \param character array
//! \param args
//!
//! Add start byte at the beginning of the msg buffer and end byte and the
//! end of the msg buffer then send message over UART
//!
//! \return None
//
//*****************************************************************************
void UartApp_messageFrame(const char *_format, ...);

//*****************************************************************************
//
//! Creates PWM JSON string
//!
//! \param pointer to character array
//! \param channel A
//! \param channel B
//! \param channel C
//!
//! Takes in the value of the 3 PWM channels and creates a JSON string which
//! is put at the input pointer, max JSON string length is defined above
//!
//! \return None
//
//*****************************************************************************
void UartApp_createPwmMessage(char *string, float chan_a, float chan_b, float chan_c);

//*****************************************************************************
//
//! Test JSON message over UART
//!
//! \param None
//!
//! Send 3 channel sine waves out of phase from each other in JSON packet in infinite loop
//!
//! \return None
//
//*****************************************************************************
void UartApp_testJsonComms();

#endif /* SPECTRE_APPLICATION_LAYER_INCLUDE_UARTAPP_H_ */
