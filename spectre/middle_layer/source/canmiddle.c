/*
 * canmiddle.c
 *
 *  Created on: Mar. 22, 2021
 *      Author: mkamalel
 */

//! \file   ~/spectre/middle_layer/source/canmiddle.c
//! \brief  Contains the middle layer of CAN
//! \brief  CanMiddle (CanMiddle)
//!

#include "canmiddle.h"
#include "canapp.h"
#include <stdint.h>
#include "interrupt.h"

//! \brief     CAN interrupt service routine
__interrupt void canISR(void);


uint16_t CanMiddle_init()
{
    // Initialize GPIO and configure GPIO pins for CANTX/CANRX
    GPIO_setPinConfig(DEVICE_GPIO_CFG_CANRXA);
    GPIO_setPinConfig(DEVICE_GPIO_CFG_CANTXA);

    // Initialize the CAN controller
    CAN_initModule(CANA_BASE);

    // Set up the CAN bus bit rate to 250kHz
    CAN_setBitRate(CANA_BASE, DEVICE_SYSCLK_FREQ, 250000, 20);

    // Enable interrupts on the CAN peripheral.
    CAN_enableInterrupt(CANA_BASE, CAN_INT_IE0 | CAN_INT_ERROR |
                        CAN_INT_STATUS);

    // Map interrupt to canISR function
    Interrupt_register(INT_CANA0, &canISR);

    // Enable the CAN interrupt signal
    Interrupt_enable(INT_CANA0);

    // Enable the CAN interrupt signal
    CAN_enableGlobalInterrupt(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

    // Initialize the transmit message object used for sending status update messages.
    // Message Object Parameters:
    //      Message Object ID Number: 6
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Transmit
    //      Message ID Mask: 0x0
    //      Message Object Flags: None
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, STATUS_UPDATE_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_TX, 0, 0,
                           STATUS_UPDATE_MSG_LEN);
   
    // Initialize the Receive message object used for receiving torque request messages.
    // Message Object Parameters:
    //      Message Object ID Number: 1
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Receive
    //      Message ID Mask: 0x0
    //      Message Object Flags: Receive Interrupt
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, TORQUE_REQUEST_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
                           TORQUE_REQUEST_MSG_LEN);

    // Initialize the Receive message object used for receiving status query messages.
    // Message Object Parameters:
    //      Message Object ID Number: 2
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Receive
    //      Message ID Mask: 0x0
    //      Message Object Flags: Receive Interrupt
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, STATUS_QUERY_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
                           STATUS_QUERY_MSG_LEN);
   
    // Initialize the Receive message object used for receiving alive messages.
    // Message Object Parameters:
    //      Message Object ID Number: 3
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Receive
    //      Message ID Mask: 0x0
    //      Message Object Flags: Receive Interrupt
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, ALIVE_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
                           ALIVE_MSG_LEN);

    // Initialize the Receive message object used for receiving parameter setting messages.
    // Message Object Parameters:
    //      Message Object ID Number: 4
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Receive
    //      Message ID Mask: 0x0
    //      Message Object Flags: Receive Interrupt
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, PARAMETER_SETTING_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
                           PARAMETER_SETTING_MSG_LEN);


    // Initialize the Receive message object used for receiving shutdown messages.
    // Message Object Parameters:
    //      Message Object ID Number: 5
    //      Message Identifier: 0x1
    //      Message Frame: Standard
    //      Message Type: Receive
    //      Message ID Mask: 0x0
    //      Message Object Flags: Receive Interrupt
    //      Message Data Length: 8 Bytes
    CAN_setupMessageObject(CANA_BASE, SHUTDOWN_MSG_ID, VCU_ID, CAN_MSG_FRAME_STD,
                           CAN_MSG_OBJ_TYPE_RX, 0, CAN_MSG_OBJ_RX_INT_ENABLE,
                           SHUTDOWN_MSG_LEN);


    CAN_startModule(CANA_BASE);

    // Uncomment to enable external loopback testmode, 
    // sent CAN msg gets received on lowest receive msg ID
    //CAN_enableTestMode(CANA_BASE, CAN_TEST_EXL);

    return 0;
}


uint16_t CanMiddle_sendStatusUpdate()
{
    //TODO: Get status from different parts of the inverter
    uint16_t txMsgData[8] = {1};

    CAN_sendMessage(CANA_BASE, STATUS_UPDATE_MSG_ID, STATUS_UPDATE_MSG_LEN,
                        txMsgData);

    return 0;
}


__interrupt void canISR(void)
{
    uint32_t status;
    uint16_t rxMsgData[8];

    // Read the CAN interrupt status to find the cause of the interrupt
    status = CAN_getInterruptCause(CANA_BASE);

    // If the cause is a controller status interrupt, then get the status
    if(status == CAN_INT_INT0ID_STATUS)
    {
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors
        status = CAN_getStatus(CANA_BASE);

        // Check to see if an error occurred.
        if(((status  & ~(CAN_STATUS_TXOK | CAN_STATUS_RXOK)) != 7) &&
           ((status  & ~(CAN_STATUS_TXOK | CAN_STATUS_RXOK)) != 0))
        {
            // TODO: CAN peripheral error handling
        }
    }
    else
    {
        CAN_readMessage(CANA_BASE, status, rxMsgData);

        CAN_clearInterruptStatus(CANA_BASE, status);

        CanApp_processMessage(status, rxMsgData);
    }

    // Clear the global interrupt flag for the CAN interrupt line
    CAN_clearGlobalInterruptStatus(CANA_BASE, CAN_GLOBAL_INT_CANINT0);

    // Acknowledge this interrupt located in group 9
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);
}
