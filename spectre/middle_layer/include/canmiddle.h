/*
 * can_frame.h
 *
 *  Created on: Mar. 22, 2021
 *      Author: mkamalel
 */

//! \file   ~/spectre/middle_layer/include/canmiddle.h
//! \brief  Contains the middle layer of CAN
//! \brief  CanMiddle (CanMiddle)
//!

#ifndef SPECTRE_MIDDLE_LAYER_INCLUDE_CANMIDDLE_H_
#define SPECTRE_MIDDLE_LAYER_INCLUDE_CANMIDDLE_H_

#include <stdint.h>
#include "device.h"

//*****************************************************************************
//
// Defines for Message library
//
//*****************************************************************************
//------Receive Messages----------
// Torque Request
#define TORQUE_REQUEST_MSG_ID 1
#define TORQUE_REQUEST_MSG_LEN 8

// Status Query
#define STATUS_QUERY_MSG_ID 2
#define STATUS_QUERY_MSG_LEN 8

// Alive Signal
#define ALIVE_MSG_ID 3
#define ALIVE_MSG_LEN 8

// Parameter Setting
#define PARAMETER_SETTING_MSG_ID 4
#define PARAMETER_SETTING_MSG_LEN 8

// Shutdown
#define SHUTDOWN_MSG_ID 5
#define SHUTDOWN_MSG_LEN 8

//------Send Messages----------
// Status Update
#define STATUS_UPDATE_MSG_ID 6
#define STATUS_UPDATE_MSG_LEN 8

//*****************************************************************************
//
// Miscellaneous Defines
//
//*****************************************************************************
#define VCU_ID 0x1

// **************************************************************************
// the typedefs
//*****************************************************************************

//! \brief Defines the CAN Middle status codes
//!
typedef enum {
    MESSAGE_CREATED = 0,
    MESSAGE_FAILED
} CanMiddle_STATUS_CODE;

//*****************************************************************************
//
//! Initializes middle layer of CAN
//!
//! \param None
//!
//! Sets up GPIO pins for CAN, sets up interrupts, creates message boxes for the
//! CAN library of the Spectre X90, and starts the CAN peripheral
//!
//! \return Status code of CAN initialization
//
//*****************************************************************************
uint16_t CanMiddle_init();

//*****************************************************************************
//
//! Send status update of inverter over CAN
//!
//! \param None
//!
//! Send Status update CAN message
//!
//! \return Status of CAN message send
//
//*****************************************************************************
uint16_t CanMiddle_sendStatusUpdate();
#endif /* SPECTRE_MIDDLE_LAYER_INCLUDE_CANMIDDLE_H_ */
