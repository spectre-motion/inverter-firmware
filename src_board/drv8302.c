/*
 * drv8302.c
 *
 *  Created on: May 20, 2021
 *      Author: junaidkhan
 */
// the includes

#include <math_include/math.h>
#include "drv8302.h"
#include "HAL_files/hal.h"

// **************************************************************************
// modules

// **************************************************************************
// platforms

// **************************************************************************
// the defines

// **************************************************************************
// the globals

// **************************************************************************
// the function prototypes

void DRV8302_enable(DRV8302_Handle handle)
{
    DRV8302_Obj *obj = (DRV8302_Obj *)handle;
    volatile uint16_t enableWaitTimeOut;
    uint16_t n = 0;

    // Enable the DRV8302
    GPIO_writePin(obj->gpioNumber_EN, 1);

    enableWaitTimeOut = 0;

    // Make sure the FAULT bit is not set during startup
    while(((GPIO_readPin(HAL_PM_nFAULT_GPIO) &
            DRV8302_FAULT) != 0) && (enableWaitTimeOut < 1000))
    {
        if(++enableWaitTimeOut > 999)
        {
            obj->enableTimeOut = true;
        }
    }

    // Wait for the DRV8302 to go through start up sequence
    for(n = 0; n < 0xffff; n++)
    {
        __asm(" NOP");
    }

    return;
} // end of DRV8302_enable() function

DRV8302_Handle DRV8302_init(void *pMemory)
{
    DRV8302_Handle handle;

    // assign the handle
    handle = (DRV8302_Handle)pMemory;

    return(handle);
} // end of DRV8302_init() function

void DRV8302_setGPIONumber(DRV8302_Handle handle, uint32_t gpioNumber_EN, uint32_t gpioNumber_FAULT)
{
    DRV8302_Obj *obj = (DRV8302_Obj *)handle;

    // initialize the gpio interface object
    obj->gpioNumber_EN = gpioNumber_EN;

    // initialize the gpio interface object
    obj->gpioNumber_FLT = gpioNumber_FAULT;

    return;
} // end of DRV8320_setGPIONumber() function



